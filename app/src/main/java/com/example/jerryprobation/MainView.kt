package com.example.jerryprobation

interface MainView {
    fun startProgress()
    fun stopProgress()
    fun onStateChanaged(state: ServerlessPeerConnectionClient.State)
    fun callConnected()
    fun callDisconnected()
    fun reportError(msg: String)
    fun makeToast(msg: String)
    fun chatEstablished()
    fun receiveMessage(msg: String)
}