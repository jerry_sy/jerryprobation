package com.example.jerryprobation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ChattingRecyclerViewAdapter: RecyclerView.Adapter<ChattingRecyclerViewAdapter.ChattingViewHolder>() {
    private val items = ArrayList<String>()

    inner class ChattingViewHolder(v : View): RecyclerView.ViewHolder(v){
        private var view = v
        fun bind(str: String){
            view.findViewById<TextView>(R.id.chatting_item_tv).text = str
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChattingViewHolder {
        val inflatedView = LayoutInflater.from(parent.context).inflate(R.layout.chatting_recyclerview_item, parent, false)
        return ChattingViewHolder(inflatedView)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ChattingViewHolder, position: Int) {
        holder.bind(items[position])
    }

    fun addItem(str: String){
        items.add(str)
        notifyDataSetChanged()
    }
}