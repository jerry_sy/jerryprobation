package com.example.jerryprobation

import android.content.Context
import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.webrtc.*
import org.webrtc.audio.AudioDeviceModule
import org.webrtc.audio.JavaAudioDeviceModule
import java.io.File
import java.nio.ByteBuffer
import java.util.regex.Pattern
import kotlin.coroutines.CoroutineContext
import kotlin.text.Charsets.UTF_8

class ServerlessPeerConnectionClient(
    val rootEglBase: EglBase,
    val context: Context,
    val peerConnectionParameters: PeerConnectionParameters,
    val view: MainView
) : CoroutineScope {

    val VIDEO_TRACK_ID = "ARDAMSv0"
    val AUDIO_TRACK_ID = "ARDAMSa0"
    val VIDEO_TRACK_TYPE = "video"
    private val HD_VIDEO_WIDTH = 640
    private val HD_VIDEO_HEIGHT = 480
    private val VIDEO_CODEC_VP8 = "VP8"
    private val VIDEO_CODEC_VP9 = "VP9"
    private val VIDEO_CODEC_H264 = "H264"
    private val VIDEO_CODEC_H264_BASELINE = "H264 Baseline"
    private val VIDEO_CODEC_H264_HIGH = "H264 High"
    private val AUDIO_CODEC_OPUS = "opus"

    private val job = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private val iceServers = arrayListOf(
        PeerConnection.IceServer("stun:14.63.228.248"),
        PeerConnection.IceServer(
            "turn:192.158.29.39:3478?transport=udp",
            "28224511:1379330808",
            "JZEOEt2V3Qb0y27GRntt2u2PAYA="
        )
    )

    private var factory: PeerConnectionFactory? = null
    private var peerConnection: PeerConnection? = null
    private var dataChannel: DataChannel? = null
    private val pcObserver = PCObserver()
    private val sdpObserver = SdpObserver()


    private var isInitiator: Boolean = false
    private var isError: Boolean = false
    private var videoCaptureStopped: Boolean = false

    private var localRender: VideoSink? = null
    private var remoteSinks: List<VideoSink>? = null

    private var videoCapturer: VideoCapturer? = null
    private var surfaceTextureHelper: SurfaceTextureHelper? = null
    private val audioConstraints = MediaConstraints()
    private var localAudioTrack: AudioTrack? = null
    private var audioSource: AudioSource? = null
    private var localVideoTrack: VideoTrack? = null
    private var remoteVideoTrack: VideoTrack? = null
    private var localVideoSender: RtpSender? = null

    private var videoSource: VideoSource? = null
    private lateinit var sdpMediaConstraints: MediaConstraints

    private var videoWidth = 0
    private var videoHeight = 0
    private var videoFps = 0

    private val db = FirebaseFirestore.getInstance()
    private var roomNum: String? = null
    private var localSdp: SessionDescription? = null
    private var remoteSdp: SessionDescription? = null
    private var localCandidates = JSONArray()
    private var remoteCandidates: ArrayList<IceCandidate>? = null

    enum class State {
        WAITING_FOR_CALL,
        WAITING_FOR_ANSWER,
        WAITING_FOR_CONNECT,
        PROCESSING,
        CONNECTED

    }

    var state: State = State.WAITING_FOR_CALL
        set(value) {
            field = value
            view.onStateChanaged(value)
        }


    data class PeerConnectionParameters(
        val videoWidth: Int,
        val videoHeight: Int,
        val videoCodec: String
    )

    init {
        launch {
            isInitiator = false
            PeerConnectionFactory.initialize(
                PeerConnectionFactory.InitializationOptions.builder(context)
                    .setEnableInternalTracer(true)
                    .createInitializationOptions()
            )
        }

    }

    //-----------------------
    fun onClickCall(roomNumber: String) {
        roomNum = roomNumber
        state = State.PROCESSING
        val docRef = db.collection("room").document(roomNum!!)
        remoteCandidates = arrayListOf<IceCandidate>()

        docRef.get().let {
            it.addOnSuccessListener { doc ->
                if (doc.exists()) { //Room Exists
                    val roomData = doc.data
                    if (roomData?.get("answer") != null) { //when room is full
                        Log.d("JerryTag", "Room is Full")
                        view.makeToast("Room is FULL")
                        state = State.WAITING_FOR_CALL
                        return@addOnSuccessListener
                    }

                    //join process
                    Log.d("JerryTag", "Room exist. Join room: $roomNum")

                    peerConnection?.let { pc ->

                        val offerData = roomData?.get("offer")
                        if (offerData != null) {//db already have offer
                            Log.d("JerryTag", "Room has offer")
                            val offerJson = JSONObject(offerData.toString())
                            val sdp = offerJson.getString("sdp")
                            launch {
                                withContext(Dispatchers.IO) {
                                    pc.setRemoteDescription(
                                        sdpObserver,
                                        SessionDescription(SessionDescription.Type.OFFER, sdp)
                                    )
                                    remoteSdp =
                                        SessionDescription(SessionDescription.Type.OFFER, sdp)
                                }
                            }
                        } else { //do not have so add snapshot
                            docRef.addSnapshotListener { snapshot, e ->
                                if (e != null) {
                                    Log.d("JerryTag", "Listen Failed. ", e)
                                    return@addSnapshotListener
                                }
                                Log.d("JerryTag", "snapshot catch offer")
                                launch {
                                    val offerJson =
                                        JSONObject(snapshot?.data?.get("offer").toString())
                                    val sdp = offerJson.getString("sdp")
                                    withContext(Dispatchers.IO) {
                                        pc.setRemoteDescription(
                                            sdpObserver,
                                            SessionDescription(
                                                SessionDescription.Type.OFFER,
                                                sdp
                                            )
                                        )
                                        remoteSdp =
                                            SessionDescription(
                                                SessionDescription.Type.OFFER,
                                                sdp
                                            )
                                    }
                                }
                            }


                        }
                    }

                } else {
                    //no room
                    Log.d("JerryTag", "NO room")

                    launch {
                        peerConnection?.let { pc ->
                            if (!isError) {
                                isInitiator = true
                                withContext(Dispatchers.IO) {
                                    //create Empty room
                                    db.collection("room").document(roomNum!!)
                                        .set(
                                            hashMapOf(
                                                "offer" to null,
                                                "answer" to null
                                            )
                                        )
                                        .addOnSuccessListener {
                                            Log.d("JerryTag", "room created success")
                                            pc.createOffer(sdpObserver, sdpMediaConstraints)
                                        }
                                        .addOnFailureListener { exception ->
                                            Log.d("JerryTag", "error create room", exception)
                                            destroy()
                                            view.callDisconnected()
                                        }

                                    //create empty offer candidates db
                                    db.collection("candidates").document(roomNum + "_offer")
                                        .set(hashMapOf("offerCandidate" to null))
                                        .addOnSuccessListener {
                                            Log.d("JerryTag", "offer db created success")
                                        }
                                        .addOnFailureListener { exception ->
                                            Log.d("JerryTag", "offer db create error", exception)
                                            destroy()
                                        }

                                    //create empty answer candidates db
                                    db.collection("candidates").document(roomNum + "_answer")
                                        .set(hashMapOf("answerCandidate" to null))
                                        .addOnSuccessListener {
                                            Log.d("JerryTag", "answer dn created success")
                                        }
                                        .addOnFailureListener { exception ->
                                            Log.d("JerryTag", "answer db create error", exception)
                                            destroy()
                                        }
                                }
                            }

                        }
                    }
                }
            }
            it.addOnFailureListener { exception ->
                Log.d("JerryTag", "get failed with ", exception)
            }
        }


    }

    fun onClickSend(msg: String) {
        launch {
            if (dataChannel == null || dataChannel?.state() == DataChannel.State.OPEN || dataChannel?.state() == DataChannel.State.CONNECTING) {
                val sendJSON = JSONObject()
                sendJSON.put("message", msg)
                val buffer = ByteBuffer.wrap(sendJSON.toString().toByteArray(UTF_8))
                withContext(Dispatchers.IO) {
                    Log.d("JerryTag", "onClickSend] $sendJSON")
                    dataChannel?.send(DataChannel.Buffer(buffer, false))
                }
            } else {
                Log.d("JerryTag", "Channel is not connecting. state: ${dataChannel?.state()}")
            }

        }

    }


    fun sessionDescriptionToJSON(sessDesc: SessionDescription): JSONObject {
        val json = JSONObject()
        json.put("type", sessDesc.type.canonicalForm())
        json.put("sdp", sessDesc.description)
        return json
    }


    fun createPeerConnectionFactory(options: PeerConnectionFactory.Options) {
        if (factory != null) {
            Log.d("JerryTag", "factory is not null $factory")
            return
        } else {
            launch {
                createPeerConnectionFactoryInternal(options)
            }

        }

    }

    private fun createPeerConnectionFactoryInternal(options: PeerConnectionFactory.Options) {
        isError = false
        PeerConnectionFactory.startInternalTracingCapture(context.getExternalFilesDir(null)?.absolutePath + File.separator + "rtc-trace.txt")
        val adm = createJavaAudioDevice()
        factory = PeerConnectionFactory.builder()
            .setOptions(options)
            .setAudioDeviceModule(adm)
            .setVideoEncoderFactory(
                DefaultVideoEncoderFactory(
                    rootEglBase.eglBaseContext,
                    false,
                    false
                )
            )
            .setVideoDecoderFactory(DefaultVideoDecoderFactory(rootEglBase.eglBaseContext))
            .createPeerConnectionFactory()

        adm.release()

    }

    fun createPeerConnection(
        localRender: VideoSink,
        remoteSinks: List<VideoSink>,
        videoCapturer: VideoCapturer
    ) {

        this.localRender = localRender
        this.remoteSinks = remoteSinks
        this.videoCapturer = videoCapturer

        launch {
            createMediaConstraintsInternal()
            createPeerConnectionInternal()

        }

    }

    private fun createPeerConnectionInternal() {
        factory?.let {
            Log.d("JerryTag", "PC createPeerConnectionInternal called")
            val rtcConfig = PeerConnection.RTCConfiguration(iceServers).apply {
                //tcp config setting인 것 같

                //tcpCandidatePolicy = PeerConnection.TcpCandidatePolicy.ENABLED
                bundlePolicy = PeerConnection.BundlePolicy.MAXBUNDLE
                rtcpMuxPolicy = PeerConnection.RtcpMuxPolicy.REQUIRE
                continualGatheringPolicy =
                    PeerConnection.ContinualGatheringPolicy.GATHER_CONTINUALLY

                // keyType = PeerConnection.KeyType.ECDSA
                enableDtlsSrtp = true
                sdpSemantics = PeerConnection.SdpSemantics.UNIFIED_PLAN


            }
            peerConnection = it.createPeerConnection(rtcConfig, pcObserver)
            isInitiator = false

            val mediaStreamLabels = listOf("ARDAMS")
            //if videoenabled
            peerConnection?.addTrack(createVideoTrack(videoCapturer!!), mediaStreamLabels)
            remoteVideoTrack = getRemoteVideoTrack()
            remoteVideoTrack?.let { videoTrack ->
                videoTrack.setEnabled(true)
                for (remoteSink in remoteSinks!!) {
                    videoTrack.addSink(remoteSink)
                }
            }
            peerConnection?.addTrack(createAudioTrack(), mediaStreamLabels)
            findVideoSender()

            launch {
                withContext(Dispatchers.IO) {
                    val initiation = DataChannel.Init()
                    initiation.id = 623
                    dataChannel = peerConnection?.createDataChannel("test", initiation)?.apply {
                        registerObserver(ChannelObserver(this))
                    }
                }
            }

            Log.d("JerryTag", "peerConnection is created")

        }
        return

    }

    fun close() {
        launch {
            closeInternal()
        }
    }

    private fun closeInternal() {
        Log.d("JerryTag", "close internal call")

        peerConnection = peerConnection?.let {
            it.dispose()
            null
        }
        //audioSource
        audioSource = audioSource?.let {
            it.dispose()
            null
        }
        //videocapturer
        videoCapturer = videoCapturer?.let {
            videoCaptureStopped = true
            it.dispose()
            null
        }
        //videosource
        videoSource = videoSource?.let {
            it.dispose()
            null
        }
        //surfaceTextureHelper
        surfaceTextureHelper = surfaceTextureHelper?.let {
            it.dispose()
            null
        }

        localRender = null
        remoteSinks = null

        factory = factory?.let {
            it.dispose()
            null
        }
        if (rootEglBase.hasSurface()) {
            rootEglBase.release()
        }
        PeerConnectionFactory.stopInternalTracingCapture()
        PeerConnectionFactory.shutdownInternalTracer()

    }

    inner class PCObserver : PeerConnection.Observer {
        override fun onIceCandidate(candidate: IceCandidate?) {
            Log.d("JerryTag", "label:${candidate?.sdpMLineIndex},id:${candidate?.sdpMid}${candidate?.sdp}")
            candidate?.let {
                val json = JSONObject().apply {
                    put("label", it.sdpMLineIndex)
                    put("id", it.sdpMid)
                    put("candidate", it.sdp)
                }
                if (isInitiator) { //caller
                    db.collection("candidates").document(roomNum + "_offer")
                        .update("offerCandidate", json.toString())
                        .addOnCompleteListener {
                            Log.d("JerryTag", "update offerCandidate success")
                        }
                        .addOnFailureListener {
                            Log.d("JerryTag", "update offerCandidate fail")
                        }
                } else {
                    db.collection("candidates").document(roomNum + "_answer")
                        .update("answerCandidate", json.toString())
                        .addOnCompleteListener {
                            Log.d("JerryTag", "update answerCandidate success")
                        }
                        .addOnFailureListener {
                            Log.d("JerryTag", "update answerCandidate fail")
                        }
                }
            }
        }

        override fun onDataChannel(dc: DataChannel?) {
            Log.d("JerryTag", "data channel ${dc?.label()} established")
            dataChannel = dc
        }

        override fun onIceConnectionReceivingChange(receiving: Boolean) {
            Log.d("JerryTag", "IceConnection Receiving Change : {$receiving}")
        }

        override fun onIceConnectionChange(newState: PeerConnection.IceConnectionState?) {
            Log.d("JerryTag", "IceConnection Change : $newState")
            launch {
                if (newState == PeerConnection.IceConnectionState.CONNECTED) {
                    state = State.CONNECTED
                    view.callConnected()

                } else if (newState == PeerConnection.IceConnectionState.DISCONNECTED || newState == PeerConnection.IceConnectionState.CLOSED) {
                    destroy()
                }
            }
        }


        override fun onConnectionChange(newState: PeerConnection.PeerConnectionState?) {
            Log.d("JerryTag", "Connection Change : $newState")
        }

        override fun onIceGatheringChange(newState: PeerConnection.IceGatheringState?) {
            Log.d("JerryTag", "Gathering Change : $newState")
        }

        override fun onAddStream(stream: MediaStream?) {
            Log.d("JerryTag", "onAddStream Called")
        }

        override fun onSignalingChange(newState: PeerConnection.SignalingState?) {
            Log.d("JerryTag", "Signaling Change : $newState")
        }

        override fun onIceCandidatesRemoved(candidates: Array<out IceCandidate>?) {
        }

        override fun onRemoveStream(stream: MediaStream?) {
        }

        override fun onRenegotiationNeeded() {
        }

        override fun onAddTrack(receiver: RtpReceiver?, mediaStreams: Array<out MediaStream>?) {

        }

    }

    //create answer create offer-> mine
    private inner class SdpObserver : org.webrtc.SdpObserver {
        override fun onCreateSuccess(origSdp: SessionDescription?) {
            if (localSdp != null) {
                Log.d("JerryTag", "Multiple SDP")
                return
            }


            //sdp를 변형하면 setLocalDescription null 남.. 어떻게 바뀌는지 확인 필요
//            var sdpDescription: String = origSdp!!.description
//            sdpDescription = preferCodec(sdpDescription, VIDEO_CODEC_VP8, false)
//            sdpDescription = preferCodec(sdpDescription, AUDIO_CODEC_OPUS, true)
//            sdpDescription = setStartBitrate(VIDEO_CODEC_VP8, sdpDescription, 1000).toString()
//            val sdp = SessionDescription(origSdp.type, sdpDescription)
//            db.collection("test").add(hashMapOf("orig" to origSdp.description,"description" to sdpDescription))

            localSdp = origSdp
            Log.d("JerryTag", "sdp updated")
            val docRef = db.collection("room").document(roomNum!!)

            if (localSdp!!.type == SessionDescription.Type.OFFER) { //Caller Side
                docRef.update("offer", sessionDescriptionToJSON(localSdp!!).toString())
                    .addOnCompleteListener {
                        Log.d("JerryTag", "update offer success")
                        db.collection("candidates").document(roomNum!! + "_answer")
                            .addSnapshotListener { snapshot, e ->
                                if (e != null) {
                                    Log.d("JerryTag", "Listen Failed. ", e)
                                    return@addSnapshotListener
                                }
                                if (snapshot != null && snapshot.exists()) {
                                    val answerCandidate = snapshot.data?.get("answerCandidate")

                                    Log.d("JerryTag", "snapshot catch answerCandidate: $answerCandidate")

                                    if (answerCandidate != null) {
                                        val candidate = toJavaCandidate(JSONObject(answerCandidate.toString()))
                                        if (remoteCandidates != null) {
                                            remoteCandidates!!.add(candidate)
                                        } else {
                                            peerConnection!!.addIceCandidate(candidate)
                                        }

                                    }

                                }
                            }

                        state = State.WAITING_FOR_ANSWER
                        docRef.addSnapshotListener { snapshot, e ->
                            if (e != null) {
                                Log.d("JerryTag", "Listen Failed. ", e)
                                return@addSnapshotListener
                            }
                            if (snapshot != null && snapshot.exists()) {
                                val answerData = snapshot.data?.get("answer")
                                answerData?.let {
                                    Log.d("JerryTag", "snapshot catch answer")

                                    state = State.PROCESSING
                                    peerConnection?.let { pc ->
                                        launch {
                                            withContext(Dispatchers.IO) {
                                                pc.setLocalDescription(sdpObserver, localSdp!!)
                                            }
                                        }
                                        val answerJson = JSONObject(answerData.toString())
                                        val sdp = answerJson.getString("sdp")
                                        remoteSdp = SessionDescription(
                                            SessionDescription.Type.ANSWER,
                                            sdp
                                        )
                                    }

                                }

                            }
                        }


                    }
                    .addOnFailureListener {
                        Log.d("JerryTag", "update offer error", it)
                    }

            } else { //Callee
                docRef.update("answer", sessionDescriptionToJSON(localSdp!!).toString())
                    .addOnCompleteListener {
                        Log.d("JerryTag", "update answer success")
                        db.collection("candidates").document(roomNum!! + "_offer")
                            .addSnapshotListener { snapshot, e ->
                                if (e != null) {
                                    Log.d("JerryTag", "Listen Failed. ", e)
                                    return@addSnapshotListener
                                }
                                val offerCandidate = snapshot?.data?.get("offerCandidate")

                                Log.d("JerryTag", "snapshot catch offerCandidate: $offerCandidate")
                                if (offerCandidate != null) {
                                    val candidate = toJavaCandidate(JSONObject(offerCandidate.toString()))
                                    if(remoteCandidates != null){
                                        remoteCandidates!!.add(candidate)
                                    } else {
                                        peerConnection?.addIceCandidate(candidate)
                                    }
                                }

                            }
                        peerConnection?.setLocalDescription(sdpObserver, localSdp)
                    }
                    .addOnFailureListener {
                        Log.d("JerryTag", "update answer error", it)
                    }

            }


        }


        override fun onSetSuccess() {
            if (peerConnection == null) return

            if (isInitiator) { //caller
                if (peerConnection!!.remoteDescription == null) {
                    //set LocalDescription success
                    Log.d("JerryTag", "set local description success")
                    peerConnection!!.setRemoteDescription(sdpObserver, remoteSdp)
                } else {
                    //set remoteDescription success,
                    Log.d("JerryTag", "set remote description success")
                    drainRemoteCandidates()
                }
            } else { //callee
                if (peerConnection?.localDescription != null) {
                    //set LocalDescription success
                    Log.d("JerryTag", "set local description success")
                    drainRemoteCandidates()
                } else {
                    //set remoteDescription success
                    peerConnection?.let { pc ->
                        launch {
                            Log.d("JerryTag", "set remote description success")
                            withContext(Dispatchers.IO) {
                                pc.createAnswer(sdpObserver, sdpMediaConstraints)
                            }
                        }
                    }


                }
            }
        }

        override fun onCreateFailure(error: String?) {
            Log.d("JerryTag", "create Sdp error => $error")
            launch {
                state = State.WAITING_FOR_CALL
            }

        }

        override fun onSetFailure(error: String?) {
            Log.d("JerryTag", "set Sdp error => $error")
            launch {
                state = State.WAITING_FOR_CALL
            }

        }
    }


    //stopVideoSource -> Activity onStop일 때
    fun stopVideoSource() {
        launch {
            videoCapturer?.let {
                if (!videoCaptureStopped) {
                    videoCapturer?.stopCapture()
                    videoCaptureStopped = true
                }
            }
        }

    }

    //startVideoSource -> Activity onStart일 때
    fun startVideoSource() {
        launch {
            videoCapturer?.let {
                if (videoCaptureStopped) {
                    videoCapturer?.startCapture(videoWidth, videoHeight, videoFps)
                    videoCaptureStopped = false
                }
            }
        }

    }

    private inner class ChannelObserver(private val channel: DataChannel) : DataChannel.Observer {
        override fun onMessage(buf: DataChannel.Buffer?) {
            launch {
                val buffer = buf?.data
                buffer?.let {
                    val byteArray = ByteArray(buffer.remaining())
                    buffer.get(byteArray)
                    val received = String(byteArray, UTF_8)
                    Log.d("JerryTag", "onMessage] $received")
                    try {
                        val message = JSONObject(received).getString("message")
                        view.receiveMessage(message)
                    } catch (e: JSONException) {
                        Log.d("JerryTag", "Malformed message received")
                    }
                }
            }

        }

        override fun onBufferedAmountChange(amount: Long) {
        }

        override fun onStateChange() {
            Log.d("JerryTag", "Channel state Change] ${channel.state().name}")
            launch {
                if (channel.state() == DataChannel.State.OPEN) {
                    //Chat established
                    view.chatEstablished()
                } else {
                    //Chat ended
                }
            }
        }

    }

    private fun drainRemoteCandidates() {
        if(remoteCandidates != null){
            Log.d("JerryTag", "drain candidates with ${remoteCandidates!!.size} candidates")
            peerConnection?.let { pc ->
                launch {
                    withContext(Dispatchers.IO) {
                        remoteCandidates!!.forEach { candidate ->
                            pc.addIceCandidate(candidate)
                        }
                        remoteCandidates = null

                    }
                }
            }
        }

    }

    private fun toJavaCandidate(json: JSONObject): IceCandidate {
        return IceCandidate(json.getString("id"), json.getInt("label"), json.getString("candidate"))
    }

    private fun createJavaAudioDevice(): AudioDeviceModule {
        val audioRecordErrorCallback = object : JavaAudioDeviceModule.AudioRecordErrorCallback {
            override fun onWebRtcAudioRecordInitError(p0: String?) {
                Log.d("JerryTag", "ERROR] $p0")
            }

            override fun onWebRtcAudioRecordError(p0: String?) {
                Log.d("JerryTag", "ERROR] $p0")
            }

            override fun onWebRtcAudioRecordStartError(
                p0: JavaAudioDeviceModule.AudioRecordStartErrorCode?,
                p1: String?
            ) {
                Log.d("JerryTag", "ERROR] $p1")
            }
        }

        val audioTrackErrorCallback = object : JavaAudioDeviceModule.AudioTrackErrorCallback {
            override fun onWebRtcAudioTrackError(p0: String?) {
                Log.d("JerryTag", "ERROR] $p0")
            }

            override fun onWebRtcAudioTrackStartError(
                p0: JavaAudioDeviceModule.AudioTrackStartErrorCode?,
                p1: String?
            ) {
                Log.d("JerryTag", "ERROR] $p1")
            }

            override fun onWebRtcAudioTrackInitError(p0: String?) {
                Log.d("JerryTag", "ERROR] $p0")
            }

        }

        val audioRecordStateCallback = object : JavaAudioDeviceModule.AudioRecordStateCallback {
            override fun onWebRtcAudioRecordStop() {
            }

            override fun onWebRtcAudioRecordStart() {
            }

        }

        val audioTrackStateCallback = object : JavaAudioDeviceModule.AudioTrackStateCallback {
            override fun onWebRtcAudioTrackStop() {
            }

            override fun onWebRtcAudioTrackStart() {
            }

        }

        return JavaAudioDeviceModule.builder(context)
            .setAudioRecordErrorCallback(audioRecordErrorCallback)
            .setAudioTrackErrorCallback(audioTrackErrorCallback)
            .setAudioRecordStateCallback(audioRecordStateCallback)
            .setAudioTrackStateCallback(audioTrackStateCallback)
            .createAudioDeviceModule()

    }

    private fun createMediaConstraintsInternal() {
        //영상 해상도 설정
        // 디폴트 hd (1280*720) 30fps로 고정
        videoWidth = HD_VIDEO_WIDTH
        videoHeight = HD_VIDEO_HEIGHT
        videoFps = 30

        //if no audio processing
        // audioConstraints add something

        //create sdp mediaconstraint
        sdpMediaConstraints = MediaConstraints().apply {
            mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveAudio", "true"))
            mandatory.add(MediaConstraints.KeyValuePair("OfferToReceiveVideo", "true"))
            mandatory.add(MediaConstraints.KeyValuePair("DtlsSrtpKeyAgreement", "true"))
        }


    }

    private fun createAudioTrack(): AudioTrack? {
        audioSource = factory?.createAudioSource(audioConstraints)
        localAudioTrack = factory?.createAudioTrack(AUDIO_TRACK_ID, audioSource)
        localAudioTrack?.setEnabled(true)

        return localAudioTrack
    }

    private fun createVideoTrack(capturer: VideoCapturer): VideoTrack? {
        surfaceTextureHelper =
            SurfaceTextureHelper.create("CaptureThread", rootEglBase.eglBaseContext)
        videoSource = factory?.createVideoSource(capturer.isScreencast)
        capturer.apply {
            initialize(surfaceTextureHelper, context, videoSource?.capturerObserver)
            startCapture(videoWidth, videoHeight, videoFps)
        }
        localVideoTrack = factory?.createVideoTrack(VIDEO_TRACK_ID, videoSource).apply {
            this?.setEnabled(true)
            this?.addSink(localRender)
        }

        return localVideoTrack
    }

    private fun getRemoteVideoTrack(): VideoTrack? {
        for (transceiver: RtpTransceiver in peerConnection!!.transceivers) {
            val track = transceiver.receiver.track()
            if (track is VideoTrack) return track
        }
        return null
    }


    //findVideoSender
    private fun findVideoSender() {
        for (sender in peerConnection!!.senders) {
            sender.track()?.let {
                val trackType: String = sender.track()!!.kind()
                if (trackType == VIDEO_TRACK_TYPE) {
                    localVideoSender = sender
                }
            }
        }
    }

    private fun preferCodec(sdpDescription: String, codec: String, isAudio: Boolean): String {
        val lines = ArrayList(sdpDescription.split("\r\n"))
        val mLineIndex = findMediaDescriptionLine(isAudio, lines)

        if (mLineIndex == -1) {
            Log.d("JerryTag", "No mediaDescription, so can not prefer $codec")
            return sdpDescription
        }
        val codecPayloadTypes = arrayListOf<String>()
        val codecPattern = Pattern.compile("^a=rtpmap:(\\d+) $codec(/\\d+)+[\r]?$")
        for (line in lines) {
            val codecMatcher = codecPattern.matcher(line)
            if (codecMatcher.matches()) {
                codecPayloadTypes.add(codecMatcher.group(1))
            }
        }
        if (codecPayloadTypes.isEmpty()) {
            Log.d("JerryTag", "No payload types with name $codec")
            return sdpDescription
        }

        val newMLine = movePayloadTypesToFront(codecPayloadTypes, lines[mLineIndex])
            ?: return sdpDescription
        lines[mLineIndex] = newMLine
        return joinString(lines.toList(), "\r\n", true)
    }

    private fun findMediaDescriptionLine(isAudio: Boolean, sdpLines: ArrayList<String>): Int {
        val mediDescription = when (isAudio) {
            true -> "m=audio"
            false -> "m=video"
        }
        for (i in 0 until sdpLines.size) {
            if (sdpLines[i].startsWith(mediDescription)) {
                return i
            }
        }
        return -1

    }

    private fun movePayloadTypesToFront(
        preferredPayloadTypes: List<String>,
        mLine: String
    ): String? {
        val origLineParts: List<String> = mLine.split(" ")
        if (origLineParts.size <= 3) {
            Log.d("JerryTag", "Wrong media description format")
            return null
        }
        val header = origLineParts.subList(0, 3)
        val unpreferredTypes = (origLineParts.subList(3, origLineParts.size)).toMutableList()
        unpreferredTypes.removeAll(preferredPayloadTypes)
        val newLineParts = arrayListOf<String>().apply {
            addAll(header)
            addAll(preferredPayloadTypes)
            addAll(unpreferredTypes)
        }
        return joinString(newLineParts, " ", false)

    }

    private fun joinString(
        s: Iterable<CharSequence?>,
        delimiter: String,
        delimiterAtEnd: Boolean
    ): String {
        val iter = s.iterator()
        if (!iter.hasNext()) {
            return ""
        }
        val buffer = StringBuilder(iter.next()!!)
        while (iter.hasNext()) {
            buffer.append(delimiter).append(iter.next())
        }
        if (delimiterAtEnd) {
            buffer.append(delimiter)
        }
        return buffer.toString()
    }

    private fun getSdpVideoCodecName(parameters: PeerConnectionParameters): String {
        return when (parameters.videoCodec) {
            VIDEO_CODEC_VP8 -> VIDEO_CODEC_VP8
            VIDEO_CODEC_VP9 -> VIDEO_CODEC_VP9
            VIDEO_CODEC_H264_HIGH, VIDEO_CODEC_H264_BASELINE -> VIDEO_CODEC_H264
            else -> VIDEO_CODEC_VP8
        }
    }


    private fun setStartBitrate(
        codec: String,
        sdpDescription: String,
        bitrateKbps: Int
    ): String? {
        val lines = sdpDescription.split("\r\n").toTypedArray()
        var lineIndex = -1
        var codecRtpMap: String? = null
        // a=rtpmap:<payload type> <encoding name>/<clock rate> [/<encoding parameters>]
        val regex = "^a=rtpmap:(\\d+) $codec(/\\d+)+[\r]?$"
        val codecPattern = Pattern.compile(regex)
        for (i in lines.indices) {
            val codecMatcher = codecPattern.matcher(lines[i])
            if (codecMatcher.matches()) {
                codecRtpMap = codecMatcher.group(1)
                lineIndex = i
                break
            }
        }
        if (codecRtpMap == null) {
            return sdpDescription
        }

        val newSdpDescription = java.lang.StringBuilder()
        for (i in lines.indices) {
            newSdpDescription.append(lines[i]).append("\r\n")
            if (i == lineIndex) {
                val bitrateSet = ("a=fmtp:" + codecRtpMap
                        + " x-google-start-bitrate=" + bitrateKbps)
                newSdpDescription.append(bitrateSet).append("\r\n")
            }
        }
        return newSdpDescription.toString()
    }

    fun destroy() {
        roomNum?.let {
            db.collection("candidates").document(it + "_answer").delete()
                .addOnSuccessListener {
                    Log.d("JerryTag", "answer candidates db successfully deleted!")
                }
                .addOnFailureListener { e ->
                    Log.d("JerryTag", "Error deleting room document", e)
                }

            db.collection("candidates").document(it + "_offer").delete()
                .addOnSuccessListener {
                    Log.d("JerryTag", "offer candidates db successfully deleted!")
                }
                .addOnFailureListener { e ->
                    Log.d("JerryTag", "Error deleting candidates document", e)
                }

            db.collection("room").document(it).delete()
                .addOnSuccessListener {
                    Log.d("JerryTag", "room successfully deleted!")
                    dataChannel?.close()
                    coroutineContext.cancelChildren()
                    view.callDisconnected()
                }
                .addOnFailureListener { e ->
                    Log.d("JerryTag", "Error deleting candidates document", e)
                }

        }

    }


}