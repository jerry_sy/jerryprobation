package com.example.jerryprobation

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings

import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

import org.webrtc.*


class MainActivity : AppCompatActivity(), MainView {
    private val REQ_CODE_PERMISSION = 2

    private val permissionList = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.RECORD_AUDIO
    )

    private var retainInstance: Boolean = false
    private var pcClient: ServerlessPeerConnectionClient? = null
    lateinit var peerConncetionParameters: ServerlessPeerConnectionClient.PeerConnectionParameters

    private var activityRunning = false
    private var isError = false

    private val adapter = ChattingRecyclerViewAdapter()


    private inner class ProxyVideoSink : VideoSink {
        private var target: VideoSink? = null
        override fun onFrame(frame: VideoFrame?) {
            synchronized(this) {
                target?.onFrame(frame)
            }
        }

        fun setTarget(target: VideoSink?) {
            synchronized(this) {
                this.target = target
            }
        }

    }

    private val remoteProxyRenderer: ProxyVideoSink = ProxyVideoSink()
    private val localProxyVideoSink: ProxyVideoSink = ProxyVideoSink()
    private var remoteSinks = arrayListOf<VideoSink>()

    private var fullscreenRenderer: SurfaceViewRenderer? = null
    private var pipRenderer: SurfaceViewRenderer? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        checkPermission()

        val eglBase = EglBase.create()



        //create renderers
        fullscreenRenderer = a_main_fullscreen_video_view
        pipRenderer = a_main_pip_video_view

        pipRenderer?.apply {
            init(eglBase.eglBaseContext, null)
            setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT)
            setZOrderMediaOverlay(true)
            setEnableHardwareScaler(true)
        }

        fullscreenRenderer?.apply {
            init(eglBase.eglBaseContext, null)
            setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL)
            setEnableHardwareScaler(false)
        }

        remoteSinks.add(remoteProxyRenderer)


        val retainedClient = lastCustomNonConfigurationInstance as ServerlessPeerConnectionClient?
        if (retainedClient == null) {
            //width height -> 임의 지정해주
            //PeerConnectionclient를 먼저 생성해준다음에기
            // it is possible to call inner class constructor after create outer class instance

            try {
                peerConncetionParameters =
                    ServerlessPeerConnectionClient.PeerConnectionParameters(640, 480, "H264")
                pcClient = ServerlessPeerConnectionClient(
                    eglBase,
                    applicationContext,
                    peerConncetionParameters,
                    this
                )
                val options: PeerConnectionFactory.Options = PeerConnectionFactory.Options()
                pcClient?.createPeerConnectionFactory(options)
            } catch (e: Exception) {
                Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
                e.printStackTrace()
            }
        } else {
            pcClient = retainedClient
            onStateChanaged(pcClient!!.state)
        }


        a_main_call_btn.setOnClickListener {
            pcClient?.createPeerConnection(localProxyVideoSink, remoteSinks, createVideoCapturer()!!)

            val roomNumber = a_main_room_number_et.text.toString()
            if(roomNumber.isNotEmpty()){
                pcClient?.onClickCall(roomNumber)
            }
            a_main_room_number_et.text.clear()
        }

        //Chatting
        a_main_chatting_rv.adapter = adapter
        a_main_chatting_rv.layoutManager = LinearLayoutManager(this@MainActivity)


        a_main_chat_send_btn.setOnClickListener {
            val msg = a_main_chat_et.text.toString()
            Log.d("JerryTag","send msg is : $msg")
            pcClient?.onClickSend(msg)
            adapter.addItem("you: $msg")
            a_main_chat_et.text.clear()
        }


        a_main_stop_btn.setOnClickListener {
            pcClient!!.close()
            disconnect()
        }

    }

    private fun disconnect() {
        Log.d("JerryTag","Checking] disconnect Called")
        activityRunning = false
        remoteProxyRenderer.setTarget(null)
        localProxyVideoSink.setTarget(null)
        if(pipRenderer != null){
            pipRenderer!!.release()
            pipRenderer = null
        }
        if (fullscreenRenderer != null) {
            fullscreenRenderer!!.release()
            fullscreenRenderer = null
        }
        if (pcClient != null) {
            pcClient!!.close()
            pcClient = null
        }

        if (!isError) {
            setResult(Activity.RESULT_OK)
        } else {
            setResult(Activity.RESULT_CANCELED)
        }
        finish()

    }
    //-----------permission check

    @TargetApi(Build.VERSION_CODES.M)
    fun checkPermission(): Boolean {
        val permissionDeniedList: MutableList<String> = arrayListOf()
        for (permission in permissionList) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionDeniedList.add(permission)
            }
        }

        if (permissionDeniedList.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                permissionDeniedList.toTypedArray(),
                REQ_CODE_PERMISSION
            )
            return true
        }
        return false
    }

    @TargetApi(Build.VERSION_CODES.M)
    fun checkResultPermission() {
        val permissionDeniedList: MutableList<String> = arrayListOf()
        for (permission in permissionList) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionDeniedList.add(permission)
            }
        }

        if (permissionDeniedList.isNotEmpty()) {
            Log.d("JerryTag", permissionDeniedList.toString())
            showSettingPermission()
            a_main_call_btn.isEnabled = false
        }
    }

    @Override
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQ_CODE_PERMISSION) {
            val grantResult = getPermissionResult(permissions, grantResults)
            if (grantResult == null) {
                finish()
                return
            }

            for (permission in grantResult) {
                if (permission != PackageManager.PERMISSION_GRANTED) {
                    Log.d("JerryTag", permission.toString())
                    showSettingPermission()
                    a_main_call_btn.isEnabled = false
                }
                return

            }
        }

        a_main_call_btn.isEnabled = false
    }


    @Override
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_CODE_PERMISSION) {
            checkResultPermission()
            return
        }
        finish()
    }

    private fun showSettingPermission() {
        val popup = AlertDialog.Builder(this).apply {
            setTitle("Permission Request")
            setMessage("Allow all permissions in app permission settings.")
            setNegativeButton("exit") { dialog, which ->
                finish()
            }
            setPositiveButton("setting") { dialog, which ->
                val intent = Intent().apply {
                    action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    data = Uri.fromParts("package", packageName, null)
                }
                ActivityCompat.startActivityForResult(
                    this@MainActivity,
                    intent,
                    REQ_CODE_PERMISSION,
                    null
                )
            }

        }
        popup.show()
    }

    private fun getPermissionResult(
        permissions: Array<out String>,
        grantResults: IntArray
    ): MutableList<Int>? {
        var currentString = ""
        val list: MutableList<Int> = arrayListOf()

        if (permissions.size != grantResults.size) return null

        val arrayLength: Int = permissions.size
        for (i in 0 until arrayLength) {
            if (currentString != permissions[i]) {
                list.add(grantResults[i])
                currentString = permissions[i]
            }
        }
        return list
    }


    //check permission

    //onrequestPermissionResult

    //showPermissionSetting -> startActivityForResult (request code permission으로 붙여서 넣어야 함)

    //onActivityResult

    //permission 없으면 콜 버튼 안 보여주고 다 granted되면 버튼 visible

    //client들 만들기


    //세팅 정보값들 체크해서 인텐트 세팅하는 함수
    //startActivityForResult()
    override fun onRetainCustomNonConfigurationInstance(): Any? {
        retainInstance = true
        Log.d("JerryTag","Checking] onRetainCustomNonfigurationInstance Called")
        return pcClient
    }


    override fun callConnected() {
        Log.d("JerryTag", "Call connected")
        remoteProxyRenderer.setTarget(pipRenderer)
        localProxyVideoSink.setTarget(fullscreenRenderer)
        fullscreenRenderer?.setMirror(true)
        pipRenderer?.setMirror(true)

        a_main_stop_btn.isEnabled = true
        a_main_chatting_gruop.visibility = View.VISIBLE
        a_main_call_gruop.visibility = View.INVISIBLE

    }

    override fun callDisconnected() {
        disconnect()
    }

    override fun reportError(msg: String) {
        if (!isError) {
            isError = true
            Log.d("JerryTag", "Error With Message. message: $msg")
        }

        isError = true
    }


    override fun startProgress() {
        a_main_pb.visibility = View.VISIBLE
        a_main_stop_btn.isEnabled = false
    }

    override fun stopProgress() {
        a_main_pb.visibility = View.INVISIBLE
        a_main_stop_btn.isEnabled = true
    }

    override fun onStateChanaged(state: ServerlessPeerConnectionClient.State) {
            Log.d("JerryTag", "myState: $state")
            if (state == ServerlessPeerConnectionClient.State.PROCESSING) {
                a_main_call_gruop.isEnabled = false
                startProgress()
            } else {
                stopProgress()
                when(state){
                    ServerlessPeerConnectionClient.State.WAITING_FOR_CALL -> {
                        a_main_call_gruop.visibility = View.VISIBLE
                        a_main_chatting_gruop.visibility = View.INVISIBLE

                    }
                    ServerlessPeerConnectionClient.State.WAITING_FOR_ANSWER -> {
                        a_main_call_gruop.visibility = View.INVISIBLE
                        a_main_chatting_gruop.visibility = View.VISIBLE
                        adapter.addItem("WAITING FOR CONNECT...")
                    }
                    ServerlessPeerConnectionClient.State.WAITING_FOR_CONNECT -> {
                        a_main_call_gruop.visibility = View.INVISIBLE
                        a_main_chatting_gruop.visibility = View.VISIBLE
                    }
                    else -> return
                }
            }
    }

    override fun makeToast(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }

    override fun chatEstablished() {
        a_main_chatting_gruop.visibility = View.VISIBLE
        adapter.addItem("Peer2Peer Connected")
    }

    override fun receiveMessage(msg: String) {
        adapter.addItem("sender: $msg")
        //tts 여기서 호출..
    }


    private fun useCamera2(): Boolean {
        //return Camera2Enumerator.isSupported(this) && intent.getBooleanExtra(EXTRA_CAMERA2, true)
        return Camera2Enumerator.isSupported(this)
    }


    private fun createCameraCapturer(enumerator: CameraEnumerator): VideoCapturer? {
        val deviceNames: Array<String> = enumerator.deviceNames

        // facing camera
        for (deviceName in deviceNames) {
            if (enumerator.isFrontFacing(deviceName)) {
                val videoCapturer = enumerator.createCapturer(deviceName, null)
                if (videoCapturer != null) return videoCapturer
            }
        }

        // other camera
        for (deviceName in deviceNames) {
            if (!enumerator.isFrontFacing(deviceName)) {
                val videoCapturer = enumerator.createCapturer(deviceName, null)
                if (videoCapturer != null) return videoCapturer
            }
        }

        return null
        //카메라가 아예 없을 때
    }

    private fun captureToTexture(): Boolean {
        //return intent.getBooleanExtra(EXTRA_CAPTURETOTEXTURE_ENABLED, false)
        return true
    }

    private fun createVideoCapturer(): VideoCapturer? {
        val videoCapturer: VideoCapturer? =
            if (useCamera2()) {
                if (!captureToTexture()) {
                    Log.d("JerryTag", "Error:camera2 texture only")
                    //reportError("camera2 texture only")
                    return null
                }
                createCameraCapturer(Camera2Enumerator(this))
            } else {
                createCameraCapturer(Camera1Enumerator(captureToTexture()))
            }
        if (videoCapturer == null) {
            Log.d("JerryTag", "Error:failed to open camera")
            //reportError("failed to open camera")
            return null
        }
        return videoCapturer
    }

    override fun onStop() {
        super.onStop()
        activityRunning = false
        pcClient?.stopVideoSource()
        pcClient?.destroy()
        disconnect()
        activityRunning = false
    }

    override fun onStart() {
        super.onStart()
        activityRunning = true
        pcClient?.startVideoSource()
    }

}
